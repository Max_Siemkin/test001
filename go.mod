module bitbucket.org/Max_Siemkin/test001

go 1.13

require (
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.7.0
)
