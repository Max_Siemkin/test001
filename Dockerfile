FROM golang:stretch as go
RUN apt-get update && apt-get install -y ca-certificates
ADD . /go/src/bitbucket.org/Max_Siemkin/test001
WORKDIR /go/src/bitbucket.org/Max_Siemkin/test001
RUN CGO_ENABLED=0 GO111MODULE=on GOOS=linux GARCH=amd64 go build -mod vendor -a -installsuffix cgo
ENTRYPOINT ["./test001"]