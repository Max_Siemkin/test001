package main

import (
	"bytes"
	"encoding/json"
	goflag "flag"
	"fmt"
	"io/ioutil"
	golog "log"
	"net/http"
	"net/url"
	"os"

	"github.com/sirupsen/logrus"
	flag "github.com/spf13/pflag"
	"github.com/spf13/viper"
)

func init() {
	golog.SetOutput(ioutil.Discard)

	viper.SetConfigFile("config.yaml")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	flag.CommandLine.AddGoFlagSet(goflag.CommandLine)
	flag.PrintDefaults()
	flag.Parse()

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	viper.AutomaticEnv()

	err = viper.BindPFlags(flag.CommandLine)
	if err != nil {
		panic(err)
	}

	err = viper.WriteConfig()
	if err != nil {
		panic(err)
	}
}
func main() {
	// Init logger
	logrus.SetFormatter(&logrus.JSONFormatter{})
	logrus.SetOutput(os.Stdout)
	logrus.SetReportCaller(true)
	level, err := logrus.ParseLevel(viper.GetString("logs.level"))
	if err != nil {
		panic(err)
	}
	logrus.SetLevel(level)
	// Check webhook-url
	whStr := viper.GetString("telegram.webhook")
	whUrl, err := url.Parse(whStr)
	if err != nil {
		logrus.WithError(err).Fatal("wrong webhook url: %s", whStr)
	}
	// Start wh-listener
	go func() {
		http.ListenAndServeTLS(
			":443",
			viper.GetString("service.cert_file"),
			viper.GetString("service.key_file"),
			http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				body, err := ioutil.ReadAll(r.Body)
				if err != nil {
					logrus.WithError(err).Error("can`t read body from request")
					return
				}
				// Do something
				fmt.Println(string(body))
			}))
	}()
	// Set webhooks
	for _, token := range viper.GetStringSlice("telegram.tokens") {
		resp, err := req(token, "setWebhook", &SetWebhook{URL: whUrl.String()})
		if err != nil {
			logrus.WithError(err).Fatal("can`t set webhook")
		}
		logrus.Debug(string(resp))
	}
	// Wait
	done := make(chan struct{})
	<-done
}

type SetWebhook struct {
	URL            string   `json:"url,omitempty"`
	Certificate    []byte   `json:"certificate,omitempty"`
	MaxConnections int      `json:"max_connections,omitempty"`
	AllowedUpdates []string `json:"allowed_updates,omitempty"`
}

func req(token, method string, data interface{}) ([]byte, error) {
	body, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest(http.MethodPost, fmt.Sprintf("https://api.telegram.org/bot%s/%s", token, method), bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("wrong response status: %d (%s)", resp.StatusCode, string(respBody))
	}
	return respBody, nil
}
